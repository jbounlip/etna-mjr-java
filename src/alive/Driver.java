package alive;

import vroom.Vehicule;

public interface Driver {
	public abstract void drive(Vehicule v) throws Exception;
	public abstract void accelerate(Vehicule v) throws Exception;
	public abstract void brake(Vehicule v) throws Exception;
}
