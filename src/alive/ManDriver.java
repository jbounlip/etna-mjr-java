package alive;

import vroom.Vehicule;

public class ManDriver extends Man implements Driver {

	public ManDriver(String name) {
		super(name);
	}

	@Override
	public void drive(Vehicule v) throws Exception {
		v.isPassengerIsInto(this);
		v.drive();
	}

	@Override
	public void accelerate(Vehicule v) throws Exception {
		v.isPassengerIsInto(this);
		v.accelerate(25);
	}

	@Override
	public void brake(Vehicule v) throws Exception {
		v.isPassengerIsInto(this);
		v.brake(25);
	}

}
