package alive;

public abstract class Human {
	private String name;
	protected String gender;
	
	public Human(String name) {
		this.name = name;
	}
	
	public abstract void drink();
	public abstract void eat();
	
	public String getName() {
		return name;
	}
}
