package alive;

import vroom.Vehicule;

public class WomanDriver extends Woman implements Driver {

	public WomanDriver(String name) {
		super(name);
	}

	@Override
	public void drive(Vehicule v) throws Exception {
		v.isPassengerIsInto(this);
		v.drive();
	}

	@Override
	public void accelerate(Vehicule v) throws Exception {
		v.isPassengerIsInto(this);
		v.accelerate(15);
	}

	@Override
	public void brake(Vehicule v) throws Exception {
		v.isPassengerIsInto(this);
		v.brake(15);
	}

}
