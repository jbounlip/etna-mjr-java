package vroom;

import java.util.ArrayList;
//import java.util.Vector;

import java.util.List;

import alive.Human;

public abstract class Vehicule {
	private String name;
	private List<Human> passengers;
	// private Vector<Human> passengers;
	protected float currentGas;
	protected float gasCapacity;
	protected float gasConsumption;
	protected int speed;
	protected int passagerCapacity;

	public Vehicule(String name) {
		this.name = name;
		this.passengers = new ArrayList<Human>();
		this.currentGas = 0;
		this.gasCapacity = 0;
		this.gasConsumption = 0;
		this.speed = 0;
		this.passagerCapacity = 0;
	}

	abstract public void accelerate(int speed);

	abstract public void brake(int speed);

	public void refuel() {
		this.currentGas = this.gasCapacity;
	}

	public void drive() throws Exception {
		//try {
			float consumption = (this.gasConsumption * this.speed) / 100;
			if (this.currentGas < consumption)
				throw new Exception("Pas assez d'essence !");
			else {
				this.currentGas -= consumption;
				System.out.printf("Le vehicule a consomme : %.2fL\n",
						consumption);
			}
		//} catch (Exception e) {
		//	System.out.println(e.getMessage());
		//}
	}

	public void addPassenger(Human h) throws Exception {
		if (this.passagerCapacity <= this.passengers.size())
			throw new Exception("Le v�hicule est plein !");
		this.passengers.add(h);
	}

	public void isPassengerIsInto(Human h) throws Exception {
		if (!this.passengers.contains((Human) h)) {
			throw new Exception("La personne n'est pas dans la voiture !");
		}
	}

	public String getName() {
		return name;
	}

	/**
	 * @return the currentGas
	 */
	public float getCurrentGas() {
		return currentGas;
	}

	/**
	 * @return the gasCapacity
	 */
	public float getGasCapacity() {
		return gasCapacity;
	}

	/**
	 * @return the gasConsumption
	 */
	public float getGasConsumption() {
		return gasConsumption;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @return the passagerCapacity
	 */
	public int getPassagerCapacity() {
		return passagerCapacity;
	}
}
