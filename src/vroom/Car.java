package vroom;
/**
 * 
 */

/**
 * @author jb
 *
 */
public class Car extends Vehicule {
	
	public Car(String name) {
		this(name, 50, 5);
	}
	
	public Car(String name, float gasCapacity, int peopleCapacity) {
		super(name);
		try {
			if (gasCapacity < 0 || peopleCapacity < 0) {
				throw new Exception("Parametres du constructeur invalides: (" + gasCapacity + ", " + peopleCapacity + ")");
			} else {
				this.gasCapacity = gasCapacity;
				this.passagerCapacity = peopleCapacity;
				this.speed = 0;
				this.gasConsumption = 7;
				this.refuel();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}

	@Override
	public void accelerate(int speed) {
		try {
			if (this.speed + speed > 220) {
				throw new Exception("La voiture ne peut pas d�passer 220km/h !");
			} else {
				this.speed += speed;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void brake(int speed) {
		try {
			if (this.speed - speed < 0) {
				throw new Exception("Une vitesse ne peut pas �tre n�gative !");
			} else {
				this.speed -= speed;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
