package test;

import alive.Man;
//import alive.Woman;
import alive.ManDriver;
import alive.WomanDriver;
import vroom.Car;

/**
 * 
 */

/**
 * @author jb
 * 
 */
public class Main {

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) {
		Car voiture = new Car("Twingo");
		ManDriver h = new ManDriver("Georges");
		WomanDriver f = new WomanDriver("Lily");

		System.out.println("=========== La voiture ===========");
		System.out.println("[Car] Name : " + voiture.getName());
		System.out.println("[Car] Current Gas : " + voiture.getCurrentGas());
		System.out.println("[Car] Gas Capacity : " + voiture.getGasCapacity());
		System.out.println("[Car] Gas Consumption : "
				+ voiture.getGasConsumption());
		System.out.println("[Car] Passager Capacity : "
				+ voiture.getPassagerCapacity());
		System.out.println("[Car] Speed : " + voiture.getSpeed());
		System.out.println("=========== L'homme ===========");
		System.out.println("[Driver One] Name : " + h.getName());
		System.out.println("----- Story -----");
		try {
			System.out.println("Avant: h.drive(); h.drive(voiture); ");
			h.drive();
			//h.drive(voiture);
			System.out.println("Apres: h.addPassenger() x 6 ");
			voiture.addPassenger(h);
			voiture.addPassenger(f);
			voiture.addPassenger(new Man("Tyty"));
			voiture.addPassenger(new Man("Toto"));
			voiture.addPassenger(new Man("Tete"));
			
			//voiture.addPassenger(new Woman("Titi"));
			//voiture.addPassenger(new Woman("Tutu"));
			for (int i = 0; i < 6; ++i) {
				h.accelerate(voiture);
				h.drive(voiture);
				//f.accelerate(voiture);
				//f.brake(voiture);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("----- End of Story -----");
		System.out.println("[Car] Name : " + voiture.getName());
		System.out.println("[Car] Current Gas : " + voiture.getCurrentGas());
		System.out.println("[Car] Gas Capacity : " + voiture.getGasCapacity());
		System.out.println("[Car] Gas Consumption : " + voiture.getGasConsumption());
		System.out.println("[Car] Passager Capacity : "
				+ voiture.getPassagerCapacity());
		System.out.println("[Car] Speed : " + voiture.getSpeed());
	}

}
